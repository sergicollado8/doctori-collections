<?php

namespace DoctorI\Shared\Domain\Collections;

use ArrayIterator;
use Countable;
use IteratorAggregate;

abstract class Collection implements Countable, IteratorAggregate
{
    public function __construct(private array $items = [])
    {
        Assert::arrayOf($this->type(), $this->items);
    }

    abstract protected function type(): string;

    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->items());
    }

    public function items(): array
    {
        return $this->items;
    }

    public function count(): int
    {
        return count($this->items());
    }

    public function add(mixed $element): void
    {
        Assert::instanceOf($this->type(), $element);
        $this->items[] = $element;
    }

    public function isEmpty(): bool
    {
        return $this->count() === 0;
    }
}
