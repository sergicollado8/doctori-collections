# Value objects models

![source](https://img.shields.io/badge/source-doctori/collections-blue)
![version](https://img.shields.io/badge/version-1.0.0-blue.svg)
![php](https://img.shields.io/badge/php->=8.0-8892bf)

Collections models to use in Doctor I applications.

## 🚀 Environment Setup

### 🛠️ Install dependencies
Install the dependencies if you haven't done it previously: `make build` or `composer install`

### ✅ Tests execution
Execute PHPUnit tests:
```bash
$ make run-test
```

### 🎨 Style
This project follows PSR-12 style
```bash
$ make check-style

$ make fix-style
```

### 🧐 Inspection
This project should be inspected by PHPStan
```bash
$ make inspect-phpstan
```

## 🔥 Usage

### 🛠️ Install in a third application
Include the package with composer.
```bash
$ composer require doctori/collections
```


A collection represents a group of objects. Each object in the collection is of a specific, defined type.

### Typed Collection
It is preferable to subclass \DoctorI\Shared\Domain\Collections\Collection to create your own typed collections. For example:

``` php
namespace My\Foo;

class FooCollection extends \DoctorI\Shared\Domain\Collections\Collection
{
    public function getType(): string
    {
        return 'My\Foo';
    }
}
```

And then use it similarly to the earlier example:

``` php
$fooCollection = new \My\Foo\FooCollection();
$fooCollection->add(new \My\Foo());
$fooCollection->add(new \My\Foo());

foreach ($fooCollection as $foo) {
    // Do something with $foo
}
```

One benefit of this approach is that you may do type-checking and type-hinting on the collection object.

``` php
if ($collection instanceof \My\Foo\FooCollection) {
    // the collection is a collection of My\Foo objects
}
```

#### Instantiating from an array of objects
In addition to `add`, you can also create a Typed Collection from an array of objects.

``` php
$foos = [
  new \My\Foo(),
  new \My\Foo()
];

$fooCollection = new \My\Foo\FooCollection($foos);
```
