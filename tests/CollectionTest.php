<?php

declare(strict_types=1);

namespace DoctorI\Tests\Shared\Domain\Collections;

use DoctorI\Tests\Shared\Domain\Collections\ObjectMother\AnotherObjectMother;
use DoctorI\Tests\Shared\Domain\Collections\ObjectMother\CollectionMother;
use DoctorI\Tests\Shared\Domain\Collections\ObjectMother\ObjectMother;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class CollectionTest extends TestCase
{
    private CollectionMother $collection;
    private string $exceptionMessage;

    protected function setUp(): void
    {
        $this->collection = new CollectionMother();
        $this->exceptionMessage =
            'The object <DoctorI\Tests\Shared\Domain\Collections\ObjectMother\ObjectMother> is not an instance of' .
            ' <DoctorI\Tests\Shared\Domain\Collections\ObjectMother\AnotherObjectMother>';
    }

    public function testIsEmpty(): void
    {
        self::assertTrue($this->collection->isEmpty());
    }

    public function testIsNotEmpty(): void
    {
        $this->collection->add(new ObjectMother(1));
        self::assertFalse($this->collection->isEmpty());
    }

    public function testCountCollection(): void
    {
        $this->collection->add(new ObjectMother(1));
        self::assertEquals(1, $this->collection->count());
        $this->collection->add(new ObjectMother(2));
        $this->collection->add(new ObjectMother(3));
        self::assertEquals(3, $this->collection->count());
    }

    public function testCollectionWithCustomArray(): void
    {
        $collection = new CollectionMother(
            [
                new ObjectMother(1),
                new ObjectMother(2),
                new ObjectMother(3),
            ]
        );
        self::assertEquals(3, $collection->count());
    }

    public function testCollectionWithWrongTypeInConstructor(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage($this->exceptionMessage);
        new CollectionMother(
            [
                new ObjectMother(1),
                new ObjectMother(2),
                new AnotherObjectMother(85),
            ]
        );
    }

    public function testCollectionWithWrongTypeInAdd(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage($this->exceptionMessage);
        $this->collection->add(new AnotherObjectMother(85));
    }

    public function testGetItems(): void
    {
        $this->collection->add(new ObjectMother(2));
        $this->collection->add(new ObjectMother(3));
        $iterator = $this->collection->getIterator();
        self::assertEquals($iterator->current(), new ObjectMother(2));
    }
}
