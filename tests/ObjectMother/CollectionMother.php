<?php

declare(strict_types=1);

namespace DoctorI\Tests\Shared\Domain\Collections\ObjectMother;

use DoctorI\Shared\Domain\Collections\Collection;
use DoctorI\Tests\Shared\Domain\Collections\ObjectMother\ObjectMother;

final class CollectionMother extends Collection
{

    protected function type(): string
    {
        return ObjectMother::class;
    }
}
