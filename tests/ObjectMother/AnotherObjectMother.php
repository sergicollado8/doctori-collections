<?php

declare(strict_types=1);

namespace DoctorI\Tests\Shared\Domain\Collections\ObjectMother;

final class AnotherObjectMother
{
    public function __construct(private int $id)
    {
    }
}
