<?php

declare(strict_types=1);

namespace DoctorI\Tests\Shared\Domain\Collections\ObjectMother;

final class ObjectMother
{
    public function __construct(private int $id)
    {
    }
}
